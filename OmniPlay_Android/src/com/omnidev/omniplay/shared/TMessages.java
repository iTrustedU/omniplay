package com.omnidev.omniplay.shared;

public class TMessages {
	public static final String		  	MP3_HEADER			= "MP3_HEADER";
	public static final String 	  	  	MP3_FOOTER   		= "MP3_FOOTER";
	public static final String 			MP3_MESSAGE			= "MP3_MESSAGE";
	public static final String			STRING_MESSAGE		= "STRING_MESSAGE";
}
