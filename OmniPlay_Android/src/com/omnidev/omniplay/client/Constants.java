package com.omnidev.omniplay.client;

public class Constants {
	public static final String DEVICE_FOUND 		= "android.bluetooth.device.action.FOUND";
	public static final String ADMIN_TAG 			= "ADMIN_BA";
	public static final int    SIMPLE_MESSAGE		= 0xF82FC8FD;
}
