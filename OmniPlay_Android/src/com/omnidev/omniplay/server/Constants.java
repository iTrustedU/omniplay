package com.omnidev.omniplay.server;

public class Constants {
	public static final int 			CLIENT_CONNECTED  	= 0xA1B2C3D4;
	public static final int				MAX_ALLOWED_CLIENTS	= 0x07;
	
	public static final int 			BLUETOOTH_ENABLE_DISCOVERY 	= 0xA4C2D7;
	public static final int				BLUETOOTH_DISCOVERY_TIME	= 3600; //in seconds
	
	public static final String			CLIENT_CONNECTED_STRING  = "%s connected";
}
